# Photo Mosaic

## Tiling Images

* square images
* uniformly sized to 100 pixels
* of the same format PNG (higher quality, more space) or JPEG (less information, less space)?

## Program Input

* an input image to create a mosaic from
* a directory to the tiling images
* size of tile OR size of output image? (can be inferred from tile size and vice-versa)

## Libraries

* JuicyPixels - <https://hackage.haskell.org/package/JuicyPixels-3.3.8>
* ImageMagick to pre-process images - <https://legacy.imagemagick.org/Usage/>

See other useful libraries in this article,
<https://www.stackbuilders.com/blog/image-processing/>

## Method

* get the average colour of each tiling image:
  <https://sighack.com/post/averaging-rgb-colors-the-right-way> (can this be
  recorded so as not to have to repeat the calculation?) Sum the squares of the
  RGB colour. When returning the average color, for each color component, find
  the mean of the sum (of squares) and return its square root.

* Mean squared error (MSE): This is a popular choice for measuring the
  difference between two images. It is calculated by taking the average of the
  squared differences between the corresponding pixels in the two images:

```haskell
from PIL import Image

def mse(image1, image2):
  """Calculates the mean squared error between two images."""
  image1 = image1.convert('L')
  image2 = image2.convert('L')

  diff = image1 - image2
  n = len(diff.getdata())

  return np.sum(diff.getdata())**2 / n
```

  To use this function, you would first need to load the two images into Pillow
  objects. Then, you could call the mse() function to calculate the MSE between
  the two images.
